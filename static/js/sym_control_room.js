let maxWidth = window.innerWidth;
let maxHeight = window.innerHeight;

let debug = false;

function writeMessage(message) {
    // debug
    text.text(message);
}

let stage = new Konva.Stage({
    container: 'container',   // id of container <div>
    width: maxWidth,
    height: maxHeight,
    x: 0,
    y: 0
});

let layer = new Konva.Layer();

let circle = new Konva.Circle({
    x: stage.width() / 2,
    y: stage.height() / 2,
    radius: 15, // TODO: scale to page size
    fill: 'lightgreen',
    stroke: 'black',
    strokeWidth: 2
});

let ratio = maxWidth > maxHeight ? 1376 / maxHeight : 1200 / maxWidth;
let xOffset = maxWidth > maxHeight ? (maxWidth - 1200 / ratio) / 2 : 0;
let yOffset = maxHeight > maxWidth ? (maxHeight - 1376 / ratio) / 2 : 0;

let valaro = new Image();
valaro.onload = function () {
    let valaroWidth = 0, valaroHeight = 0;
    if (maxHeight > maxWidth) {
        valaroWidth = maxWidth;
        valaroHeight = 1376 / ratio;
    } else {
        valaroWidth = 1200 / ratio;
        valaroHeight = maxHeight;
    }

    let img = new Konva.Image({
        x: xOffset,
        y: yOffset,
        image: valaro,
        width: valaroWidth,
        height: valaroHeight
    });

    layer.add(img);
    layer.add(circle);
};
valaro.src = 'valaro.png';

// add the layer to the stage
stage.add(layer);

if (debug) {
    var text = new Konva.Text({
        x: 10,
        y: 10,
        fontFamily: 'Calibri',
        fontSize: 24,
        text: '',
        fill: 'black',
    });

    layer.add(text);
}

// draw the image
layer.draw();