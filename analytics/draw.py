from core import SYMData
from enum import Enum
from math import fsum, sqrt


class SymDrawType(Enum):
    Classic = 0,
    Heatmap = 1


class SymPoint(object):

    def __init__(self, sym_data: SYMData = None, xy: [int] = None):
        if sym_data is None:    # for barycenter
            self.real_x: int = xy[0]
            self.real_y: int = xy[1]
            self.x: float = self.real_x * 2.91 + 392
            self.y: float = self.real_y * -3.06 + 434
            self.color = None  # inherited from container, custom if not None
            self.timestamp = '0'
            self.user = None
        self.real_x: int = sym_data.x
        self.real_y: int = sym_data.y
        self.x: float = sym_data.x * 2.91 + 392
        self.y: float = sym_data.y * -3.06 + 434
        self.color = None  # inherited from container, custom if not None
        self.timestamp = sym_data.timestamp
        self.user = sym_data.user_id


class SymPointGroup(object):

    def __init__(self, color: str):
        self.points: [SymPoint] = []
        self.color: str = color
        self.user = None
        self.delta = None
        self.data = None  # change that name

    def set_user(self, user_id):
        self.user = user_id
        for point in self.points:
            point.user = user_id

    def calculate_delta(self):
        """
        TODO : Check if delta is calculated on a collection of 2 points
        :return:
        """
        self.delta = SymPoint(None, [self.points[-1].x - self.points[0].x,
                                     self.points[-1].y - self.points[0].y])


class SymPointGroupSet(object):

    def __init__(self):
        self.groups: [SymPointGroup] = []
        self.barycenter = None
        self.average = None
        self.stddev = None

    def calculate_barycenter(self):
        _bary_coord = [0, 0]
        _values = []
        _lengths = 0
        for group in self.groups:
            # TODO : triple check that, it's wrong
            for point in group.points:
                assert isinstance(point, SymPoint)
                _bary_coord[0] += point.real_x / len(group.points)
                _bary_coord[1] += point.real_y / len(group.points)
                _values.append([point.real_x, point.real_y])
            _lengths += len(group)
        c_distance = [sqrt((x - _bary_coord[0]) ** 2 + (y - _bary_coord[1]) ** 2) for x, y in _values]
        self.average = fsum(c_distance) / _lengths
        cc_distance = [((x - _bary_coord[0]) ** 2 + (y - _bary_coord[1]) ** 2) for x, y in _values]
        self.stddev = sqrt((fsum(cc_distance)) / _lengths - self.average ** 2)
        self.barycenter = SymPoint(None, _bary_coord)


class SymDraw(object):
    pass

    def __init__(self):
        self.data: [SymPointGroupSet] = []
        self.payloads = []

    def add_simple_data(self, _data: [SYMData]):
        _set = SymPointGroupSet()
        _group = SymPointGroup('blue')  # TODO : get original user color value, and option for random
        for _d in _data:
            _group.points.append(SymPoint(_d))
        _set.groups.append(_group)
        self.data.append(_set)

    def add_augmented_data(self, _data: [(any, SYMData)]):
        pass

    def prepare_solo(self):
        """
        Add payloads to generate single diagrams
        :return:
        """
        if isinstance(self.data, dict):
            pass
        else:
            users = []
            for datum in self.data:
                assert isinstance(datum, SYMData)

    def prepare_heatmap(self):
        try:
            assert isinstance(self.data, dict)
        except AssertionError:
            return

        self.add_payload(type='heatmap', delta='only', cluster=True)

    def add_payload(self, **args):
        """
        Prepares the generation of the diagram.
        List of recognized flags and values :
        - type {classic, heatmap, data}
        - delta {only, append}      # only = only deltas displayed, append = appends delta to original data
        - cluster: bool             # clusters based on Delestage, Yvart, 2023
        - zones {2018, circumplex}  # 2018 based on Delestage, 2018 ; circumplex based on Delestage, Yvart, 2023
        - markers {nbr, abs, delta} # additional text right bellow each SymPoint
        - extras: str               # extra information to be displayed on the diagram
        - a_b {pre, post}           # only show first (pre) or last (post) data point of each Group
        - filters: dict             # filter to be applied on additional data. Must use dict data_input
        - group_by {user, data}      # form groups based on user_id or data provided
        :param args: flags for diagram generation, see above
        """
        self.payloads.append(args)

    def execute_payloads(self):

        for payload in self.payloads:

            if isinstance(self.data, dict):
                pass
            else:
                pass

            # data preparation

            # points drawing

            # extra drawings (vectors, etc.)

            # information drawing

            # saving

    class SymCircle(object):
        pass

    class SymVector(object):
        pass
