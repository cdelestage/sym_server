"""
SYM analytics module
Makes SYM diagrams exports as image files
"""
import json

from PIL import ImageFont
from PIL import Image
from PIL import ImageDraw
from core import SYMData, User
from datetime import datetime
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import random
import math
import os

from core.exporter import EXPORTABLE_TYPES

__author__ = "Charles-Alexandre Delestage"
__copyright__ = "2021-2024"
__credits__ = ["Charles-Alexandre Delestage"]
__license__ = "BSD-3"
__version__ = "0.2"
__maintainer__ = "Charles-Alexandre Delestage"
__email__ = "charles-alexandre.delestage@iut.u-bordeaux-montaigne.fr"
__status__ = "Development"


cwd = os.getcwd()
RESSOURCES_PATH = cwd if cwd[-9:] == 'analytics' else cwd + os.path.sep + 'analytics'
font_data = font_user = ImageFont.truetype(
    os.path.sep.join([RESSOURCES_PATH, 'fonts', 'CreatoDisplay', 'CreatoDisplay-Regular.otf']), size=18)

# Check for folders hierarchy
"""if not os.path.isdir(RESSOURCES_PATH + '/../exports'):
    os.mkdir(RESSOURCES_PATH + '/../exports')
    print('regenerated exports folder')
if not os.path.isdir(RESSOURCES_PATH + '/../exports/sym'):
    os.mkdir(RESSOURCES_PATH + '/../exports/sym')
    print('regenerated exports/sym folder')"""


def hsb2rgb(hsb):
    """
    Transforms a hsb array to the corresponding rgb tuple
    In: hsb = array of three ints (h between 0 and 360, s and v between 0 and 100)
    Out: rgb = array of three ints (between 0 and 255)
    Credit to physicalattraction @ stackoverflow
    https://stackoverflow.com/questions/4554627/rgb-to-hsv-conversion-using-pil
    """
    h = float(hsb[0] / 360.0)
    s = float(hsb[1] / 100.0)
    b = float(hsb[2] / 100.0)

    if s == 0:
        r = int(round(b * 255))
        g = int(round(b * 255))
        b = int(round(b * 255))
    else:
        var_h = h * 6
        if var_h == 6:
            var_h = 0  # H must be < 1
        var_i = int(var_h)
        var_1 = b * (1 - s)
        var_2 = b * (1 - s * (var_h - var_i))
        var_3 = b * (1 - s * (1 - (var_h - var_i)))

        if var_i == 0:
            var_r = b
            var_g = var_3
            var_b = var_1
        elif var_i == 1:
            var_r = var_2
            var_g = b
            var_b = var_1
        elif var_i == 2:
            var_r = var_1
            var_g = b
            var_b = var_3
        elif var_i == 3:
            var_r = var_1
            var_g = var_2
            var_b = b
        elif var_i == 4:
            var_r = var_3
            var_g = var_1
            var_b = b
        else:
            var_r = b
            var_g = var_1
            var_b = var_2

        r = int(round(var_r * 255))
        g = int(round(var_g * 255))
        b = int(round(var_b * 255))

    return r, g, b


def draw_vector(draw: ImageDraw, data1: SYMData, data2: SYMData, fill: tuple | str = 'red') -> None:
    """
    Draws a vector on a picture
    This function is from 2016, sorry for the lack of comments
    :param fill: color
    :param draw: draw cursor
    :param data1: tail
    :param data2: head
    """
    x1 = data1.x * 2.91 + 392
    y1 = data1.y * -3.06 + 434
    x2 = data2.x * 2.91 + 392
    y2 = data2.y * -3.06 + 434

    signe = 1
    if x1 > x2:
        signe = -1

    angle1 = math.radians(30 * signe)
    angle2 = math.radians(-30 * signe)

    if x2 != x1:
        angle1 = math.radians(30) + math.atan((y2 - y1) / (x2 - x1))
        angle2 = math.radians(-30) + math.atan((y2 - y1) / (x2 - x1))

    draw.line((x1, y1, x2, y2), fill=fill, width=3)
    draw.line((x2 - 15 * math.cos(angle1) * signe, y2 - 15 * math.sin(angle1) * signe, x2, y2), fill=fill, width=3)
    draw.line((x2 - 15 * math.cos(angle2) * signe, y2 - 15 * math.sin(angle2) * signe, x2, y2), fill=fill, width=3)


def std_text(data: SYMData, time_delta: int = None, index: int = None, coords=False) -> str:
    """
    Generates standard text for diagram about the user with different options
    Could be empty if no option is selected
    Really, time_delta is the most useful one, the other flags are more debug oriented
    :param time_delta: time delta with previous datum, not displayed if None
    :param index: index of the datum, not displayed if None
    :param data: data to be described
    :param coords: draw coordinates ?
    :return: formatted string
    """
    text = []
    if coords:
        text.append(' '.join(['x:', str(data.x), 'y:', str(data.y)]))
    if time_delta is not None:
        text.append(' '.join(['dt:', str(int(time_delta))]))
    if index is not None:
        text.append(' '.join(['n:', str(index)]))
    return '\n'.join(text)


def extract_vectors(draw: ImageDraw, user: User) -> None:
    """
    Draw vectors following the extract path
    :param draw: ImageDraw handle
    :param user: User, to extract color associated to it
    :return:
    """
    sort_data = user.data.copy()
    prev_data = sort_data.pop(0)
    _max = len(sort_data)

    for i in range(_max):
        act_data = sort_data.pop(0)
        draw_vector(draw, prev_data, act_data, fill=user.color)
        prev_data = act_data


def generate_diagram(users: [User],
                     global_vector: bool = False,
                     coords: bool = False,
                     delta_time: bool = True,
                     index: bool = False,
                     local_vectors: bool = True,
                     back: int = 1,
                     out_path: str = None,
                     extra: str = None,
                     add_delta: bool = False) -> str:
    """
    Generates diagrams
    TODO : what about changing picture resolution ? #sweatingheavely
    :param add_delta: add the calculated delta to the diagram, with the regular points
    :param extra: extra text for custom generation
    :param out_path: if one is given, path of the image to be exported
    :param index: displays the index of the datum if True
    :param delta_time: displays the delta time with previous datum if True
    :param back: selection of the Valence-Arousal diagram variant, 0 = original, 1 = zones from (Delestage, 2018)
    :param global_vector: displays a global vector if True
    :param local_vectors: display local vectors if True
    :param users: array of users
    :param coords: displays coordinates of each datum if True
    :return: absolute path to the generated diagram file
    """
    # prepare the background of the image i.e. the Valence-Arousal diagram
    valaro = None

    if back == 0:
        valaro = Image.open(
            os.path.sep.join([RESSOURCES_PATH, 'pngs', 'valaro_800.png']))  # blank Valence-Arousal diagram
    elif back == 1:
        # diagram with the zones described by (Delestage, 2018)
        valaro = Image.open(os.path.sep.join([RESSOURCES_PATH,'pngs','valaro_full_v2.png']))
    # circles radius
    r = 8
    u_text_x, u_text_y = 15, 15

    draw = ImageDraw.Draw(valaro)

    if extra:
        draw.text((10, 50), extra, font=font_user, fill='black')

    for user in users:
        # add user information on the diagram
        draw.ellipse((u_text_x - (r + 2), u_text_y - (r + 2), u_text_x + (r + 2), u_text_y + (r + 2)), fill='black')
        draw.ellipse((u_text_x - r, u_text_y - r, u_text_x + r, u_text_y + r), fill=user.color)
        draw.text((u_text_x + r * 2, u_text_y - r), str(user.user_id), font=font_user, fill='black')
        u_text_y += r * 3

        for data in user.data:
            # SYM drawing magic numbers
            # The fact is, the diagram is not centered and offsets are to be calculated each time
            # I know who did that, but I won't tell you (not me though)
            # Anyway, the smileys are not the same size nevertheless, so it's *fine*
            # Just don't mess with these :)
            xl = data.x * 2.91 + 392
            yl = data.y * -3.06 + 434
            # Add text regarding each point drawn
            data_index = user.data.index(data)
            text = std_text(data,
                            time_delta=user.get_delta_time(data_index) if delta_time else None,
                            index=data_index if index else None,
                            coords=coords)
            # TODO : [Low Priority] Text display is too low while multi-line
            (left, top, right, bottom) = draw.multiline_textbbox((xl, yl), text, font=font_data)
            draw.ellipse((xl - (r + 2), yl - (r + 2), xl + (r + 2), yl + (r + 2)), fill='black')
            draw.ellipse((xl - r, yl - r, xl + r, yl + r), fill=user.color)
            draw.text((xl - (right - left) * 2 / 3, yl + (bottom - top)), text, font=font_data, fill='black')

        # global vector = emotion induction
        if len(user.data) > 1:
            if local_vectors:
                extract_vectors(draw, user)
            if global_vector:
                draw_vector(draw, user.data[0], user.data[-1], user.color)

        if add_delta:
            user.calculate_delta()

            xl = user.sym_delta.x * 2.91 + 392
            yl = user.sym_delta.y * -3.06 + 434
            draw.ellipse((xl - (r + 2), yl - (r + 2), xl + (r + 2), yl + (r + 2)), fill='black')
            draw.ellipse((xl - r, yl - r, xl + r, yl + r), fill='purple')

    end_name = []

    if len(users) == 1:
        end_name += str(users[0].user_id)
    if global_vector:
        end_name.append('gv')
    if local_vectors:
        end_name.append('lv')
    if coords:
        end_name.append('coords')
    if delta_time:
        end_name.append('delta_time')
    if back == 1:
        end_name.append('zones')

    if out_path is None:
        filename = datetime.now().strftime('%y-%m-%d_%H-%M-%S_') + '_'.join(end_name) + '.png'
        out_path = os.path.sep.join([RESSOURCES_PATH, '..','exports','sym', filename])
    valaro.save(out_path, dpi=(300, 300))
    return out_path


def test_users_diagrams(nbr_usr: int = 1,
                        nbr_data: int = 3,
                        diagrams: bool = False,
                        biais: (int, int) = None) -> [User]:
    """
    Quick function to generate fake users and test diagram generation
    It's pretty bad, but it's okay
    :param nbr_usr: number of fake users
    :param nbr_data: number of fake data per user
    :param diagrams: whether it's for testing diagrams or not
    :param biais: tuple to represent if a certain area is to over-represent in the data
    """
    zone = (-100, 100) if biais is None else biais
    users: [User] = []
    for x in range(nbr_usr):
        temp = User(user_id=random.randint(0, 2048))
        for y in range(nbr_data):
            temp.data.append(SYMData({
                'x': random.randint(zone[0], zone[1]),
                'y': random.randint(zone[0], zone[1]),
                'id': temp.user_id,
                'timestamp': random.random() * 1000
            }))
        users.append(temp)
    if diagrams:
        generate_diagram(users)
    return users


def get_color_gradient_value(val: float) -> (int, int, int, int):
    """
    Calculates a color value regarding input value.
    The color gradient should be usable to anyone, including colorblind people.
    Input value must be in interval [0;1]
    :param val: input value
    :return: tuple representing the color in RGBA in [0..255] interval per canal
    """
    return int(255 * math.cos(math.pi / 2 * val)), int(255 * math.cos(math.pi / 2 * val)), \
        int(255 * math.sin(val * math.pi / 2)), int(255 * val)


def cart2pol(x, y):
    """
    Calculates polar coordinates from cartesian coordinates
    :param x: a boat
    :param y: a drunken sailor
    :return: polar coordinates as a tuple
    """
    rho = math.sqrt(x ** 2 + y ** 2)
    phi = math.atan2(y, x)
    return rho, phi


def sym_data_heatmap(_users: [User],
                     step: int = 5,
                     output_path: str = None,
                     extras: str = None,
                     _delta: bool = True,
                     _pre: bool = True,
                     clusters: bool = False,
                     bary: bool = False,
                     include_extra_data: bool = False,
                     extra_filter: str = '') -> str:
    """
    Custom heatmap generation based on delta from user's data.
    Clusters and barycenter should not be displayed at the same time for readability. For this reason, if both are set
    to True, barycenter will take priority and disable clusters display.
    TODO : cleanup, this one was rushed hard
    :param extra_filter:
    :param include_extra_data: exploits the user.extra_data with automatic classification. Overrides the color gradient
    to represent categories as colors, concentration is transferred to alpha. Only top classes are kept and displayed
    as legend.
    :param bary: Represent the barycenter of the data provided and its impact circles calculated
    by (Delestage, Yvart, 2023).
    :param clusters: Adds clusterization via circles calculated by (Delestage, Yvart, 2023).
    :param _pre: if True, generates diagram for the first SYMData for each user, else on the last.
    Only read if _delta is False.
    :param _delta: If True, will only generate the diagram for the delta. Else, see _pre.
    :param extras: adds extra information to the diagram and file name. Actually vital if you do have a lot of
    diagrams to generate, since the timestamp in the filename is to the second, and generation is too fast,
    meaning files are overridden if no extra identification is passed
    :param output_path: if one is given, path of the image to be exported
    :param _users: array of User
    :param step: grid's finesse
    """

    class HMCategories(object):

        def __init__(self, text):
            self.text = text
            self.total = 0
            self.color = ''

    class HMCManager(object):

        def __init__(self):
            self.cats: [HMCategories] = []

        def add(self, cat: str):
            for c in self.cats:
                if cat == c.text:
                    c.total += 1
                    break
            self.cats.append(HMCategories(cat))

        def get_color(self, text: str):
            for c in self.cats:
                if c.text == text:
                    return c.color

        def attempt_draw(self, text, local_draw, _x, _y):
            for c in self.cats:
                if text == c.text:
                    xl = _x * 2.91 + 392
                    yl = _y * -3.06 + 434
                    local_draw.ellipse((xl - (r + 2), yl - (r + 2), xl + (r + 2), yl + (r + 2)), fill='black')
                    local_draw.ellipse((xl - r, yl - r, xl + r, yl + r), fill=self.get_color(text))
                    return

        def finalize(self, drawer, avg = None, stddev = None):
            temp = []
            for c in self.cats:
                # apply the 66.7 - 95 - 99.8 rule to get the outliers
                if c.total >= math.floor(avg + 2 * stddev):
                    temp.append(c)
            self.cats = temp
            for n in range(len(self.cats)):
                # colorize groups to the most distanced colors regarding the number of groups to be displayed
                # TODO : add a colorblind safe option
                self.cats[n].color = hsb2rgb([360 * n / len(self.cats), 100, 100])
                drawer.rectangle((15, 780 - n * 20, 35, 800 - n * 20), fill=self.cats[n].color)
                drawer.text((50, 780 - n * 20), self.cats[n].text, font=font_data)

    if bary & clusters:
        clusters = False

    valaro = Image.open(os.path.sep.join([RESSOURCES_PATH, 'pngs', 'valaro_revert.png']))  # blank Valence-Arousal diagram
    heatmap = Image.new('RGBA', valaro.size, 255)
    draw = ImageDraw.Draw(heatmap, 'RGBA')
    factor = 3.07
    offset_x = 87
    offset_y = 96

    corr_range = [a for a in range(-100, 100, step)]
    corr_range[-1] = 100

    _max = 0
    _count = 0
    _avg = 0
    _polar_values = []
    _xy_values = []
    _stddev = 0
    _bary = [0, 0]
    r = 8
    _cat_manager = HMCManager()

    def prepare_data(_x, _y, _extra_data):
        nonlocal _avg, count, _count
        if include_extra_data:
            # if _user.extra_data != extra_filter:
            #   continue
            _cat_manager.add(str(_extra_data))
        _r, p = cart2pol(_x, _y)
        _avg += _r
        _polar_values.append(_r)
        count += 1
        _count += 1
        _bary[0] += _x
        _bary[1] += _y
        _xy_values.append((_x, _y))

    if _delta:
        for _user in _users:
            _user.calculate_delta()

    # pre calculus

    for xr in corr_range:
        for yr in corr_range:
            count = 0
            for _user in _users:
                if _delta:
                    if _user.sym_delta.x in range(xr, xr + step) and _user.sym_delta.y in range(yr, yr + step):
                        prepare_data(_user.sym_delta.x, _user.sym_delta.y, _user.extra_data)
                else:
                    if _pre:
                        if _user.data[0].x in range(xr, xr + step) and _user.data[0].y in range(yr, yr + step):
                            prepare_data(_user.data[0].x, _user.data[0].y, _user.extra_data)
                    else:
                        if _user.data[-1].x in range(xr, xr + step) and _user.data[-1].y in range(yr, yr + step):
                            prepare_data(_user.data[-1].x, _user.data[-1].y, _user.extra_data)
            if count > _max:
                _max = count

    if include_extra_data:
        _cat_manager.finalize(draw)
        draw.text((10, 10), extra_filter, fill='white')
    else:
        # draw color gradient
        for x in range(256):
            draw.rectangle((x + 30, 10, x + 31, 20), fill=get_color_gradient_value(x / 256))
        draw.text((10, 25), 'min', font=font_data, fill='black')
        draw.text((267, 25), 'max', font=font_data, fill='black')

    for xr in corr_range:
        for yr in corr_range:
            count = 0
            for _user in _users:
                if _delta:
                    if _user.sym_delta.x in range(xr, xr + step) and _user.sym_delta.y in range(yr, yr + step):
                        if include_extra_data:
                            _cat_manager.attempt_draw(_user.extra_data, draw, _user.sym_delta.x, _user.sym_delta.y)
                        count += 1
                else:
                    if _pre:
                        if _user.data[0].x in range(xr, xr + step) and _user.data[0].y in range(yr, yr + step):
                            if include_extra_data:
                                _cat_manager.attempt_draw(_user.extra_data, draw, _user.data[0].x, _user.data[0].y)
                            count += 1
                    else:
                        if _user.data[-1].x in range(xr, xr + step) and _user.data[-1].y in range(yr, yr + step):
                            if include_extra_data:
                                _cat_manager.attempt_draw(_user.extra_data, draw, _user.data[-1].x, _user.data[-1].y)
                            count += 1
            yr = -yr
            if not include_extra_data:
                draw.rectangle(((xr + 100) * factor + offset_x, (yr + 100) * factor + offset_y,
                                (xr + 100 + step) * factor + offset_x, (yr + 100 + step) * factor + offset_y),
                               # fill=(255, 0, 0, int(count / _max * 255)))
                               fill=get_color_gradient_value(count / _max))

    sum_images = Image.alpha_composite(valaro, heatmap)
    draw2 = ImageDraw.Draw(sum_images)

    if extras:
        draw2.text((10, 40), extras, font=font_data, fill='black')

    if bary:
        _bary = [_bary[0] / _count, _bary[1] / _count]
        c_distance = [math.sqrt((x - _bary[0]) ** 2 + (y - _bary[1]) ** 2) for x, y in _xy_values]
        _avg = math.fsum(c_distance) / _count
        cc_distance = [((x - _bary[0]) ** 2 + (y - _bary[1]) ** 2) for x, y in _xy_values]
        _stddev = math.sqrt((math.fsum(cc_distance)) / _count - _avg ** 2)

        draw2.text((20, 80), 'barycentre', font=font_data, fill='white')

    if clusters:
        _avg /= _count
        _stddev = math.sqrt((math.fsum([n ** 2 for n in _polar_values]) / _count) - _avg ** 2)
        draw2.text((20, 80), 'impact', font=font_data, fill='white')

    if _delta:
        draw2.text((20, 60), 'delta heatmap | échelle facteur 1/2', font=font_data, fill='white')
    elif _pre:
        draw2.text((20, 60), 'pre heatmap', font=font_data, fill='white')
    else:
        draw2.text((20, 60), 'post heatmap', font=font_data, fill='white')

    if bary | clusters:
        # factor 2 to catch up with real values due to delta coord reduction
        _coord_factor = 2 if _delta else 1
        draw2.text((600, 20), 'average: ' + str('{:.2f}'.format(_avg * _coord_factor)), font=font_data)
        draw2.text((600, 40), 'std_dev: ' + str('{:.2f}'.format(_stddev * _coord_factor)), font=font_data)

        # determine circles radius
        # TODO : check for validity on standard coordinates
        _cor_factor = 0.5

        _radius_avg_std0 = (_avg - _stddev) / _cor_factor * 2.91
        _radius_avg = _avg / _cor_factor * 2.91
        _radius_avg_std1 = (_avg + _stddev) / _cor_factor * 2.91
        _radius_avg_std2 = (_avg + _stddev * 2) / _cor_factor * 2.91

        draw2.text((600, 80), 'µ-s: ' + str('{:.2f}'.format((_avg - _stddev) * _coord_factor)),
                   font=font_data, fill='cyan')
        draw2.text((600, 100), 'µ: ' + str('{:.2f}'.format(_avg * _coord_factor)),
                   font=font_data, fill='white')
        draw2.text((600, 120), 'µ+s: ' + str('{:.2f}'.format((_avg + _stddev) * _coord_factor)),
                   font=font_data, fill='green')
        draw2.text((600, 140), 'µ+2s: ' + str('{:.2f}'.format((_avg + 2 * _stddev) * _coord_factor)),
                   font=font_data, fill='yellow')

        _x_center = 392
        _y_center = 434

        if bary:
            _x_center = _bary[0] * 2.91 + 392
            _y_center = _bary[1] * -3.06 + 434

        # draw circles
        # x * 2.91 + 392
        # y * -3.06 + 434
        # avg - stddev
        draw2.ellipse((_x_center - _radius_avg_std0 / 2,
                       _y_center - _radius_avg_std0 / 2,
                       _x_center + _radius_avg_std0 / 2,
                       _y_center + _radius_avg_std0 / 2), outline='cyan')
        # avg
        draw2.ellipse((_x_center - _radius_avg / 2,
                       _y_center - _radius_avg / 2,
                       _x_center + _radius_avg / 2,
                       _y_center + _radius_avg / 2), outline='white')
        # avg + stddev
        draw2.ellipse((_x_center - _radius_avg_std1 / 2,
                       _y_center - _radius_avg_std1 / 2,
                       _x_center + _radius_avg_std1 / 2,
                       _y_center + _radius_avg_std1 / 2), outline='green')
        # avg + stddev * 2
        draw2.ellipse((_x_center - _radius_avg_std2 / 2,
                       _y_center - _radius_avg_std2 / 2,
                       _x_center + _radius_avg_std2 / 2,
                       _y_center + _radius_avg_std2 / 2), outline='yellow')

    if output_path is None:
        str_fab = [os.path.sep.join([RESSOURCES_PATH,
                                     '..','exports','sym',
                                     datetime.now().strftime('%y-%m-%d_%H-%M-%S')])]
        if extras:
            str_fab.append(extras)
        str_fab.append('delta' if _delta else 'pre' if _pre else 'post')
        str_fab.append('step' + str(step))
        if clusters:
            str_fab.append('clusters')
        if bary:
            str_fab.append('bary')
        str_fab.append('heatmap.png')
        output_path = '_'.join(str_fab)
    sum_images.save(output_path, dpi=(300, 300))
    return output_path


def generate_questionnaire_dataviz(excel_file_path: str, scenario_path: str):
    """
    Provides basic data-visualizations for each column of the Questionnaires
    Extras / Comments are not treated for now
    :param excel_file_path: path of the Excel file
    :param scenario_path: path of the Scenario used (to get the questions types)
    """
    # loading Excel export to Dataframe
    df = pd.read_excel(excel_file_path)

    # retrieving Questionnaires heads
    scenario = json.load(open(scenario_path, 'r'))
    quests = [json.load(open('quest_templates/' + f.get('pattern_path'), 'r'))
              for f in scenario.get('steps')
              if f.get('type') == 'questionnaire']

    # Checking the heads types
    head_types = {}
    # Parsing questions kind at the same time
    head_quest_type = []
    for quest in quests:
        for section in quest.get('form').get('sections'):
            if section.get('type') in EXPORTABLE_TYPES:
                head_quest_type.append((section.get('tag'),
                                        section.get('type'),
                                        section.get('question'),
                                        section.get('choices')))
                if section.get('type') == 'vas':
                    head_types.update({section.get('tag'): 'int'})
                else:
                    head_types.update({section.get('tag'): 'str'})

    # Applying head types to Dataframe
    df = df.astype(head_types)

    for tag, ttype, question, choices in head_quest_type:
        if ttype == 'vas':
            # generate a violin plot
            df_temp = df[tag].dropna()
            sns.violinplot(data=df_temp, x=tag, inner="quart", cut=True)
            plt.suptitle(question)
            plt.tight_layout()
            plt.savefig('exports/figures/' + tag + '.png')
            plt.savefig('exports/figures/' + tag + '.svg')
            continue
        if ttype == 'radio':
            # generate a pie chart
            df_temp = df[tag].dropna()
            ax_temp = df_temp.plot.pie(y=tag, autopct='%.2f%%')
            ax_temp.legend(bbox_to_anchor=(1.02, 1), borderaxespad=0).set(title='')
            ax_temp.set(title=question, ylabel='')
            plt.tight_layout()
            plt.savefig('exports/figures/' + tag + '_pie.png')
            plt.savefig('exports/figures/' + tag + '_pie.svg')

            # generate a bar chart
            tag_total = df_temp[tag].sum()
            df_temp[tag + '_percent'] = df_temp[tag].apply(lambda  x : round(x / tag_total * 100, 2))
            ax_temp_2 = df_temp.plot.bar(y=tag + '_percent')
            # ax_temp_2.legend(bbox_to_anchor=(1.4, 1), borderaxespad=0, labels=['France', 'Nouvelle-Aquitaine', 'Val de Garonne'])
            ax_temp_2.set(title=question, ylabel='Pourcentage', xlabel='')
            plt.tight_layout()
            plt.savefig('exports/figures/' + tag + '_bar.png')
            plt.savefig('exports/figures/' + tag + '_bar.svg')
            continue
        if ttype == 'check':
            # transform data from multi to single
            df_qcm = pd.DataFrame()
            for c in choices:
                df_qcm[c] = df[tag].dropna().apply(lambda x: 1 if c in x else 0)
            # generate a pie chart
            df_qcm.sum().plot.pie()
            plt.suptitle(question)
            plt.tight_layout()
            plt.savefig('exports/figures/' + tag + '_pie.png')
            plt.savefig('exports/figures/' + tag + '_pie.svg')
            # generate a bar chart
            df_qcm.sum().plot.bar()
            plt.suptitle(question)
            plt.tight_layout()
            plt.savefig('exports/figures/' + tag + '_bar.png')
            plt.savefig('exports/figures/' + tag + '_bar.svg')
            continue


def test_questionnaire_dataviz(nbr_users: int):
    """
    TODO refactor with analytics package
    :param nbr_users:
    :return:
    """
    pass

def import_sym_data(filename: str):
    """
    Import SYM Data following the SYM Web Viewer format :
    activity user x y timestamp
    :param filename: name of the file to import data from
    :return: a User list
    """
    _users: [User] = []

    raw = open(filename)

    # activity user x y timestamp
    for line in raw:
        rs = line.split(' ')
        if len(rs) != 5:
            continue
        found = False
        for user in _users:
            if user.user_id == rs[1]:
                found = True
                _data_dict = {
                    'x': rs[2],
                    'y': rs[3],
                    'id': rs[1],
                    'timestamp': rs[-1]
                }
                user.data.append(SYMData(_data_dict))
        if not found:
            _user = User(rs[1])
            _data_dict = {
                'x': rs[2],
                'y': rs[3],
                'id': rs[1],
                'timestamp': rs[-1]
            }
            _user.data.append(SYMData(_data_dict))
            _users.append(_user)
    return _users


if __name__ == '__main__':
    # test_users_diagrams(nbr_usr=4, nbr_data=2, diagrams=True)
    sym_data_heatmap(test_users_diagrams(nbr_data=2, nbr_usr=500))
