[English version](README.md) [Version Française](README_FR.md) [Wiki](https://gitlab.huma-num.fr/cdelestage/sym_server/-/wikis/home)

# SYM Server
A server-oriented Spot Your Mood flavor with pre-/post-questionnaires and video player functionalities.

# Recommended hardware setup
SYM server works on the client-server principle. If you use it on the same machine or under the same wired network, usage is relatively straightforward.

Ideally, if you should use a captive portal Wi-Fi hotspot, or have a redirection set up in your Wi-Fi network so users can easily reach the server.

If not, a simple hotspot will work be require users to scan 2 QR-Codes. From experience, it is usually a headache, since scanning the first QR-Code to connect to the Wi-Fi network will seemingly not have any effect to the user, which will think nothing happened, or will notice it does not have internet suddenly and switch networks before scanning the second one that reaches the server. As I said, trouble in the way, especially when dealing with a crowd. 

# Software setup

Here is the versions of python and the main packages used in the project :

* Python>=3.10.7 (3.8 minimum)
* openpyxl=3.1.2
* Flask=2.3.2
* flask_socketio=5.3.4

# Installation and first launch

Download the code as a zip archive or using git :

```
git clone https://gitlab.huma-num.fr/cdelestage/sym_server
cd sym_server/
```

The server is based on python, so using a virtual environment is recommended. We used venv, but you can use any other solution which fists your needs.

```
# If venv is not already installed
pip install virtualenv
# Creation of the new virtual environment using venv in the current directory
python -m venv venv

# Activation of the environnement under Linux/MacOS
source venv/bin/activate
# Activation of the environnement under Windows (cmd.exe)
venv\Scripts\activate.bat
# Activation of the environnement under Windows (PowerShell)
venv\Scripts\Activate.psl

# installation of depedencies
pip install -r requirements.txt

# Launch server
python app.py
```

# Modular scenarios
You can choose how you want your protocol scenario to unfold, by generating different types of activity.
There are several types of activities and sub-activities:
* Spot Your Mood, which can take place :
  * In continuous tracking (Delestage, 2018), which can run in parallel with another activity synchronized or not with the server. In the case of video, a video player can be launched on any computer connected to the network. The start and end of the video will be recorded in the server; 
  * Single pointing (Yvart, 2019).
* Questionnaires, with different types of questions:
  * Single-choice questions ;
  * Multiple-choice questions; 
  * Visual Analog Scales;
  * Free text fields.

Content can be edited at will in the scenario.json file (check [Wiki](https://gitlab.huma-num.fr/cdelestage/sym_server/-/wikis/home) for more details).

# Questionnaire templates
You can customize the questionnaires using json templates located in the 'quest_templates' folder. Examples are provided.
Please check the [Wiki](https://gitlab.huma-num.fr/cdelestage/sym_server/-/wikis/home) for more detail.

# Dependencies
SYM Server relies on several libraries for data management, export, file serving and networking (thanks for making this possible).

Python
* [openpyxl 3.1.2](https://github.com/ericgazoni/openpyxl)
* [Flask 2.3.2](https://github.com/pallets/flask)
* [flask_socketio 5.3.4](https://github.com/miguelgrinberg/Flask-SocketIO)

Javascript
* [Bootstrap 5.2.2](https://github.com/twbs/bootstrap)
* [JQuery 3.7.1](https://github.com/jquery/jquery)
* [SocketIO 4.0.1](https://github.com/socketio/socket.io)
* [Konva 8.1.3](https://github.com/konvajs/konva)

Fonts
* [DejaVu](https://dejavu-fonts.github.io/)

# Licence
The code is placed under the BSD-3 Licence. Please check [LICENCE]() for further details. 

# More help
To have a broader view of the usage of Spot Your Mood, you can read the previous articles and thesis about its usage : 
* DELESTAGE, Charles-Alexandre et YVART, Willy, 2024. Lived experience and virtual reality: visual method of analysis based on video recordings and the Valence-Arousal diagram. Sintaxis. 15 janvier 2024. N° 12, pp. 68‑85. DOI [10.36105/stx.2024n12.07](https://doi.org/10.36105/stx.2024n12.07). 
* DELESTAGE, Charles-Alexandre et YVART, Willy, 2023. Visualisation des transferts émotionnels de cohortes – Étude de cas sur la notion de non-violence. In : H2PTM’23, La fabrique du sens à l’ère de l’information numérique : enjeux et défis. Imad Saleh, Nasreddine Bouhaï, Sylvie Leleu-Merviel, Ioan Roxin. ISTE Editions. pp. 83‑98. Systèmes d’information, web et société. ISBN 978-1-78405-983-5.
* MOUTAT, Audrey, DELESTAGE, Charles-Alexandre and BADULESCU, Cristina, 2019. Sensorialité et émotions, vers une co-construction du sens : approche qualitative de l’expérience de visite à la Cité du vin de Bordeaux. In: 87e congrès de l’ACFAS. Montréal. 2019.
* YVART, Willy, 2019. Qualification (a)verbale de l’humeur musicale : nouvelles perspectives pour la synchronisation dans l’audiovisuel. 2019. P. 911. 
* DELESTAGE, Charles-Alexandre, 2018. Paths created by an enactive-relativized approach to experience: the case of the viewing experience. In: From UXD to LivXD. Living eXperience Design. Leleu-Merviel, S. (ed). Wiley. 
* DELESTAGE, Charles-Alexandre, 2018. L’expérience émotionnelle ou la performance des programmes de télévision. L’horizon de pertinence comme déterminant de la construction de sens par le spectateur. Thèse de doctorat. Arenberg Creative Mine. 
* DELESTAGE, Charles-Alexandre and YVART, Willy, 2018. Emotional dynamics and quality of lived experience: a study of the usage of the Valence-Arousal space with the Spot Your Mood tool. In: Visual methods for research in the fields of communication. Tenerife, Spain. December 2018.
* YVART, Willy, DELESTAGE, Charles-Alexandre and LELEU-MERVIEL, Sylvie, 2016. SYM: toward a new tool in user’s mood determination. In: Proceedings of the 2016 EmoVis Conference on Emotion and Visualization. Online. Linkoping University. 2016. p. 23–28. Available from: http://dl.acm.org/citation.cfm?id=3001319
