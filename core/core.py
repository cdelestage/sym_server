"""
Core for SYM Server
Shared object declaration
"""
import json
from enum import Enum
from dataclasses import dataclass, field
from typing import Callable
import random
import secrets

__author__ = "Charles-Alexandre Delestage"
__copyright__ = "2021-2024"
__credits__ = ["Charles-Alexandre Delestage"]
__license__ = "BSD-3"
__version__ = "0.2"
__maintainer__ = "Charles-Alexandre Delestage"
__email__ = "charles-alexandre.delestage@iut.u-bordeaux-montaigne.fr"
__status__ = "Development"


class StepType(Enum):
    """
    Describes types of steps in a scenario
    """
    Questionnaire = 0,  # questionnaires, see quest_templates
    Information = 1,  # to provide additional info to the user during the experiment
    SYM = 2,  # a Spot Your Mood diagram in continuous or single point mode
    End = 3


@dataclass(order=True)
class Step(object):
    """
    Describes a step in the protocol sequence
    Do not forget to generate updated questionnaires static pages
    """
    step_type: StepType
    step_rank: int
    name: str
    template_path: str  # path of the template in the template folder
    pattern_path: str


@dataclass()
class QuestionnairePage(Step):
    template_path: str
    data_send_path: str

    def __post_init__(self):
        self.step_type = StepType.Questionnaire


@dataclass()
class InformationPage(Step):
    static_path: str

    def __post_init__(self):
        self.step_type = StepType.Information


@dataclass()
class SYMPage(Step):
    single_mode: bool = field(default=False)
    hold: bool = field(default=True)

    def __post_init__(self):
        self.step_type = StepType.SYM


@dataclass(order=True)
class SYMData(object):
    """
    Basic SYM data object
    Very similar to other SYM data implementations
    """
    x: int
    y: int
    user_id: int
    timestamp: float

    def __init__(self, data_dict):
        setattr(self, 'x', int(data_dict['x']))
        setattr(self, 'y', int(data_dict['y']))
        setattr(self, 'user_id', data_dict['id'])
        setattr(self, 'timestamp', float(data_dict['timestamp']))


@dataclass()
class Quests(object):
    data: object


@dataclass(order=True)
class User(object):
    """
    Handle for the user and its data (pre-/post-questionnaire, SYM data, user_id)
    """
    user_id: int | str
    status: Step = field(default=None)
    quests: [Quests] = field(init=False)
    data: list[SYMData] = field(init=False)
    color: tuple = field(init=False, default=(random.getrandbits(8), random.getrandbits(8), random.getrandbits(8)))
    sym_delta: SYMData | None = field(init=False, default=None)
    extra_id: str = field(default=None, init=False)
    extra_data: int | str = field(default=None, init=False)

    def __post_init__(self):
        setattr(self, 'data', [])
        self.quests = []

    def trim_data(self, start: float, end: float) -> None:
        """
        Trims the data array regarding a time period (should be the start and end of custom event / video)
        :param start: beginning time in Unix-time milliseconds
        :param end: end time in Unix-time milliseconds
        """
        for s_data in self.data:
            if s_data.timestamp < start or s_data.timestamp > end:
                self.data.remove(s_data)

    def get_delta_time(self, index: int) -> float:
        """
        Returns the delta time between datum @ index and the previous
        :param index: index of the datum
        :return: delta time in seconds
        """
        if index == 0:
            return 0
        if index >= len(self.data):
            raise IndexError
        return self.data[index].timestamp - self.data[index - 1].timestamp

    def calculate_delta(self) -> None:
        """
        Calculate the delta of the User's data array
        TODO custom selection from time spans (for sequences of a media). results to store in specific structure
        :return:
        """
        first: SYMData = self.data[0]
        last: SYMData = self.data[-1]

        setattr(self, 'sym_delta', SYMData({
            'x': int((last.x - first.x) / 2),
            'y': int((last.y - first.y) / 2),
            'id': self.user_id,
            'timestamp': 0
        }))


@dataclass(order=True)
class Experiment(object):
    """
    Handle for the experiment data
    """
    users: list[User] = field(init=False)
    video_start: float = field(init=False, default=0)
    video_end: float = field(init=False, default=0)
    video_paused: list[float] = field(init=False)

    def __post_init__(self):
        self.users = []
        self.video_paused = []

    def set_reel_start(self, start: float):
        setattr(self, 'video_start', start)

    def set_reel_end(self, end: float):
        setattr(self, 'video_end', end)

    def add_reel_pause(self, pause: float):
        self.video_paused.append(pause)

    def add_user(self, user_id: int) -> None:
        for user in self.users:
            if user.user_id == user_id:
                return
        self.users.append(User(user_id))

    def add_sym_data(self, sym_data: SYMData) -> None:
        for user in self.users:
            if user.user_id == sym_data.user_id:
                user.data.append(sym_data)

    def add_quest_data(self, data: object, user_id: int) -> None:
        for user in self.users:
            if user.user_id == user_id:
                user.quests.append(data)

    def get_user(self, user_id: int) -> User:
        for user in self.users:
            if user_id == user.user_id:
                return user


class Server(object):

    def __init__(self):
        self.experiment: Experiment = Experiment()
        self.name = ''
        self.description = ''
        self.scenario: [Step] = []
        self.video_url = ''
        self.control_key = secrets.token_urlsafe(32)

    def load_scenario(self, path: str = None):
        scenario_file = open(path if path is not None else 'scenarios/scenario.json')
        scenario_json = json.load(scenario_file)

        self.name = scenario_json['name']
        self.description = scenario_json['description']

        for step in scenario_json['steps']:
            # check video even if it's not a SYM step, could be useful somehow
            if 'video' in step:
                self.video_url = step['video']
            step_type = step['type']
            # step_order = step['order']
            if step_type == 'sym':
                step_type = StepType.SYM
                step_name = 'Spot Your Mood'
                step_path = 'sym.html'
                self.scenario.append(SYMPage(step_type, step['order'], step_name, step_path,
                                             step['single_mode'] == 'True',   # one point or continuous ?
                                             step['hold'] == 'True'))         # does it wait for signal if single mode ?
                continue
            else:
                step_type = StepType.Questionnaire
                step_name = step['name']
                step_path = step['static_file']
                step_pattern_path = step['pattern_path']
            self.scenario.append(Step(step_type, step['order'], step_name, step_path, step_pattern_path))

        self.scenario.append(Step(StepType.End, -1, 'End', '', ''))

    def next_user_step(self, user_id: int, updater: Callable):
        for user in self.experiment.users:
            if user.user_id == user_id:
                if user.status is None:
                    user.status = self.scenario[0]
                elif user.status.step_type == StepType.End:
                    return None
                elif user.status.step_rank < len(self.scenario) - 1:
                    user.status = self.scenario[user.status.step_rank]
                else:
                    user.status = Step(StepType.End, -1, '', '', '')
                updater()
                return user.status.template_path

    def get_user_status(self, user_id: int):
        for user in self.experiment.users:
            if user.user_id == user_id:
                return user.status

    def get_count(self):
        counts = map(lambda cat: ':'.join([cat.name, str(len([u for u in self.experiment.users if u.status == cat]))]),
                     self.scenario)
        return '\n'.join(counts)

    def global_next_switch(self):
        next_index = self.scenario.index([step for step in self.scenario if step.step_type == StepType.SYM][0]) + 1
        next_step = self.scenario[next_index]
        for user in self.experiment.users:
            user.status = next_step
