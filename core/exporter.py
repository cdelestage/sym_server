import json
import pickle
import copy
from time import gmtime, strftime
from openpyxl.workbook import Workbook
from core import *

__author__ = "Charles-Alexandre Delestage"
__copyright__ = "2021-2024"
__credits__ = ["Charles-Alexandre Delestage"]
__license__ = "BSD-3"
__version__ = "0.2"
__maintainer__ = "Charles-Alexandre Delestage"
__email__ = "charles-alexandre.delestage@iut.u-bordeaux-montaigne.fr"
__status__ = "Development"

"""
Export functions for the questionnaires and single-mode SYMData only (continuous SYMData is handled in a separate module)
Either used while the server is running or offline from pickle save files 
"""


EXPORTABLE_TYPES = ['check', 'radio', 'vas', 'comment', 'check_extra', 'radio_extra']


def convert_ms_to_readable(timestamp):
    #convert nanoseconds to seconds
    timestamp /= 1000000
    time_obj = gmtime(timestamp)
    return strftime('%Y-%m-%d %H:%M:%S', time_obj)


def update_fields(key: str, data: dict, sections: [dict]) -> None:
    """
    Update a specific field from the questionnaire fields with the answers
    !IMPORTANT! the export function must check for extras with the original key
    TODO : important refactoring on the headers generation. Its works for now, but code analysis should be rerun wp
    :param key: tag of the question to update
    :param data: data of the user
    :param sections: questionnaire to update
    """
    for question in sections:
        # probably a leftover
        if question.get('type') not in EXPORTABLE_TYPES:
            continue
        if question.get('tag') == key:
            # if it's an extra tag, add a line for this result
            if key + '_extra' in [k for k in data.keys()]:
                sections.append({key + '_extra': data[key + '_extra'],
                                 'question': key + '_extra',
                                 'type': question.get('type') + '_extra',
                                 'answer': data[key + '_extra'],
                                 'tag': key + '_extra'})
            # 'check' and 'radio' are easy to update, just erase propositions with the actual answers
            if question.get('type') in ['check', 'radio']:
                question['answer'] = [question.get('choices')[int(i)] for i in data[key]]
                return
            # if not a 'check' or a 'radio', it's a vas or a comment
            # simply add an 'answer' field to the question
            question.update({'answer': data[key]})
            return
    return


def update_choices(user: User, sections: [[dict]]) -> User:
    """
    Crosses the answers from the non-flattened multidict with the questionnaire templates
    The data from the answers is poured into the copied template
    The idea is to keep empty fields from the original questions that are not retained in the form multidict
    That way, each questionnaire have consistent fields for the export
    !IMPORTANT! Sections should be cleaned before calling this function
    :param user: User, what else ?
    :param sections: questionnaire templates
    :return: updated user with rebuilt sections
    """
    quests = []

    for i in range(len(user.quests)):
        current = user.quests[i].data.to_dict(flat=False)
        section = sections[i].copy()

        for s in section:
            if s.get('type') in EXPORTABLE_TYPES:
                update_fields(s.get('tag'), current, section)

        for s in section:
            if s.get('type') not in EXPORTABLE_TYPES:
                section.remove(s)

        quests.append(section)

    updated_user = User(user.user_id)
    updated_user.quests = quests
    updated_user.data = user.data
    return updated_user


def rebuild_users(users: [User],
                  templates: [str] = None) -> [User]:
    """
    Handles the rebuilding process of the users data for export
    Used in the automatic rebuilding in the export function
    Default parameters include the base questionnaires
    :param templates: list of templates paths (MUST BE IN ORDER)
    :param users: array of User straight from the Multidict non-flatten
    :return: array of rebuilt users
    """
    if templates is None:
        templates = ['quest_templates/quest_pre.json', 'quest_templates/quest_post.json']
    sections = [json.load(open(t, 'r')).get('form').get('sections') for t in templates]
    updated_users = []
    for user in users:
        # deepcopy avoids overlapping of values due to lazy programming
        updated_users.append(copy.deepcopy(update_choices(user, sections)))
    return updated_users


def build_answers_line(user: User, _head: [str], _head_index: int) -> [str]:
    line = []
    data = user.quests[_head_index]
    for key in _head:
        for section in data:
            if section['tag'] == key:
                if section['type'] in ['radio', 'check']:
                    line.append(', '.join(section['answer']))
                    continue
                else:
                    line.append(section['answer'][0])
                    continue
    return line


def build_quest_head(quest_pattern_path: str) -> list:
    heads = []
    with open('quest_templates/' + quest_pattern_path, 'r') as quest:
        quest = json.load(quest)
        for section in quest.get('form').get('sections'):
            if section.get('type') in EXPORTABLE_TYPES:
                heads.append(section.get('tag'))
                if section.get('extras'):
                    heads.append(section.get('tag') + '_extra')
    return heads


def export_to_file(users: [User], file_name: str, auto_rebuilt: bool = False) -> None:
    """
    Export the rebuilt data into an Excel file and a csv file
    :param auto_rebuilt: if True, will rebuild users with the default settings
    :param users: collection of rebuilt users (see update-choices() for more)
    :param file_name: name of the exported file, which will be in the 'exports' folder
    """
    if auto_rebuilt:
        users = rebuild_users(users)

    wb = Workbook()
    ws = wb.active

    # build the headers
    heads = ['user_id']
    # detect number of steps in scenario
    sym_count = 0
    quest_heads = []
    quest_heads_path = []
    with open('scenarios/scenario.json', 'r') as scenario:
        scenario = json.load(scenario)
        for step in scenario.get('steps'):
            if step.get('type') == 'sym' and step.get('single_mode'):
                sym_count += 1
            if step.get('type') == 'questionnaire':
                quest_heads_path.append(step.get('pattern_path'))
    # handle sym heads
    for x in range(sym_count):
        heads.append(f'datetime_{x}')
        heads.append(f'x_{x}')
        heads.append(f'y_{x}')
    # handle questionnaire heads
    for head_path in quest_heads_path:
        _quest_head = build_quest_head(head_path)
        quest_heads.append(_quest_head)
        heads += _quest_head
    # build headers
    ws.append(heads)

    # build data lines
    # TODO index out of range issue @189
    for user in users:
        line = [user.user_id]
        for x in range(sym_count):
            line += [convert_ms_to_readable(user.data[x].timestamp), user.data[x].x, user.data[x].y]
        for i in range(len(quest_heads)):
            line += build_answers_line(user, quest_heads[i], i)
        ws.append(line)

    wb.save('exports/' + file_name + '.xlsx')


if __name__ == '__main__':
    # testing
    experiment = pickle.load(open('backups/data_save2024-02-23_10-51-00.pickle', 'rb'))
    export_to_file(experiment.users, 'test', True)
