import sqlite3
import os
import traceback
import sys
import datetime

from core import SYMData, Server, StepType, User
from dataclasses import dataclass, field

# TODO implement video pauses


__author__ = "Charles-Alexandre Delestage"
__copyright__ = "2021-2024"
__credits__ = ["Charles-Alexandre Delestage"]
__license__ = "BSD-3"
__version__ = "0.2"
__maintainer__ = "Charles-Alexandre Delestage"
__email__ = "charles-alexandre.delestage@iut.u-bordeaux-montaigne.fr"
__status__ = "Development"


@dataclass()
class DBE(object):
    """
    DB handler
    Stores session-recurrent primary keys for quick inserts
    """

    id_experiment : int = field(default=False)
    step_types_sym: int = field(default=False)
    step_types_quest: int = field(default=False)
    steps: list = field(default=False)
    target: str = field(default=False, init=False)
    
    def __post_init__(self):
        self.target = datetime.datetime.now().strftime('%y-%m-%d_%H-%M-%S') + '_sym_server.db'

    def get_step_id(self, nbr: int):
        return [x[1] for x in self.steps if x[0] == nbr][0]

    def insert_question(self, q_id: int, q_type: str, _tag: str, q_text: str, choices: [str]=None, _min: str=None, _max: str=None) -> None:
        """
        Injects Question in the DB
        To be linked directly to the generator
        :param q_text: text of the question
        :param q_id: Id of the Questionnaire linked to the Question
        :param q_type: question type
        :param _tag: question tag
        :param choices: array of choices (optional)
        :param _min: minimum (optional, VAS only)
        :param _max: maximum (optional, VAS only)
        """
        try:
            print("inserting question", q_id, q_type, _tag)
            _con = sqlite3.connect(self.target)
            cr = _con.cursor()
            insert_question = f"""
            insert into Question (parent, q_type, q_tag, q_text, q_choices, q_min, q_max) 
            values ("{q_id}", "{q_type}", "{_tag}", "{q_text}", "{"|".join(choices) if choices is not None else None}", "{_min}", "{_max}") 
            """
            cr.execute(insert_question)
            _con.commit()
            _con.close()
        except sqlite3.Error as e:
            display_error(e)
        return


    def insert_quest_blob(self, quest_id: int, _user: int, blob):
        """
        Gets answers in blob JSON kinda form and gets it into the DB
        :param quest_id: id of the Questionnaire
        :param _user: user id
        :param blob: mess to clean up and insert in the db
        :return:
        """
        _dict = blob.to_dict(flat=False)
        try:
            for key in _dict.keys():
                _con = sqlite3.connect(self.target)
                cr = _con.cursor()
                get_question_declaration = f"""select * from Question where Question.q_tag = "{key}";"""
                cr.execute(get_question_declaration)
                res = cr.fetchone()
                _con.close()
                if res is not None:
                    id_quest = res[0]
                    _type = res[2]
                    _extra = _dict.get(key + '_extra')[0] if _dict.get(key + '_extra') is not None else None
                    if _type == 'radio':
                        self.insert_quest_data(id_quest, _user, _text=res[5].split("|")[int(_dict.get(key)[0])], _extra=_extra)
                    if _type == 'check':
                        sub_res = []
                        splited = res[5].split("|")
                        for _value in _dict.get(key):
                            sub_res.append(splited[int(_value)])
                        self.insert_quest_data(id_quest, _user, _arr=sub_res, _extra=_extra)
                    if _type == 'vas':
                        self.insert_quest_data(id_quest, _user, _int=int(_dict.get(key)[0]))
                    if _type == 'comment':
                        self.insert_quest_data(id_quest, _user, _text=_dict.get(key)[0])
                    if _type == 'simple':
                        self.insert_quest_data(id_quest, _user, _text=_dict.get(key)[0])
        except sqlite3.Error as e:
            display_error(e)


    def insert_quest_data(self, quest_id: int,
                          user_id: int,
                          _text: str=None,
                          _int: int=None,
                          _extra: str=None,
                          _arr: [str]=None) -> None:
        """
        Inserts user's answer in the DB
        :param quest_id: question id
        :param user_id: user id
        :param _text: value of the textual answer (optional)
        :param _int: value of the integer answer (optional)
        :param _extra: value of the extra answer (optional)
        :param _arr: array of answers (optional)
        :return:
        """
        try:
            _con = sqlite3.connect(self.target)
            cr = _con.cursor()
            insert_data = f"""insert into Answer (ans_question, ans_user, text, integer, extra, array)
             values ("{quest_id}", "{user_id}", "{_text}", "{_int}", "{_extra}", "{'|'.join(_arr) if _arr is not None else None}")"""
            cr.execute(insert_data)
            _con.commit()
            _con.close()
        except sqlite3.Error as e:
            display_error(e)
        return
    
    
    def insert_sym_data(self, sd: SYMData, step_nbr: int) -> None:
        """
    
        :param self:
        :param sd: SYMData to be stored
        :param step_nbr: number of the SYM Step (in case of multiple ones)
        :return:
        """
        try:
            _con = sqlite3.connect(self.target)
            cr = _con.cursor()
            step_id = self.get_step_id(step_nbr)
            insert_data = f"""insert into Sym_data ('x', 'y', '_user', 'timestamp', '_step')
                              values ({sd.x}, {sd.y}, {sd.user_id}, "{sd.timestamp}", {step_id})"""
            cr.execute(insert_data)
            _con.commit()
            _con.close()
        except sqlite3.Error as e:
            display_error(e)
        return
    
    
    def insert_user_id(self, user: User) -> None:
        """
    
        :param self:
        :param user:
        :return:
        """
        try:
            _con = sqlite3.connect(self.target)
            cr = _con.cursor()
            pref_user = f"""insert into User ('id_user', '_experiment') 
                            values ({user.user_id}, {self.id_experiment});"""
            cr.execute(pref_user)
            _con.commit()
            _con.close()
        except sqlite3.Error as e:
            display_error(e)
        return


    def insert_video_start(self, _video_start: int) -> None:
        try:
            _con = sqlite3.connect(self.target)
            cr = _con.cursor()
            cmd = f"""update Experiment
                        set video_start = {_video_start}
                        where id_experiment = {self.id_experiment};"""
            cr.execute(cmd)
            _con.commit()
            _con.close()
        except sqlite3.Error as e:
            display_error(e)
        return


    def insert_video_end(self, _video_end: int) -> None:
        try:
            _con = sqlite3.connect(self.target)
            cr = _con.cursor()
            cmd = f"""update Experiment
                                    set video_end = {_video_end}
                                    where id_experiment = {self.id_experiment};"""
            cr.execute(cmd)
            _con.commit()
            _con.close()
        except sqlite3.Error as e:
            display_error(e)
        return

    
    def declare_experiment(self, server: Server) -> None:
        """
        Create the experiment in db along with Questionnaire
        :param self: DBE (see object declaration)
        :param server: server object
        :return:
        """
        try:
            _con = sqlite3.connect(self.target)
            cr = _con.cursor()
            pref_exp = f"""insert into Experiment (name, description)
                           values ('{server.name}', '{server.description}')"""
            cr.execute(pref_exp)
            _con.commit()
            res = cr.execute(f"""select id_experiment 
                           from Experiment 
                           where name = '{server.name}' 
                           and description = '{server.description}'""")
            self.id_experiment = exp_id = res.fetchone()[0]
            self.steps = []

            # Populate step types
            res = cr.execute("""select id_s_type from Step_type where s_type = 'SYM'""")
            self.step_types_sym = res.fetchone()[0]

            res = cr.execute("""select id_s_type from Step_type where s_type = 'Questionnaire'""")
            self.step_types_quest = res.fetchone()[0]

            # populate steps
            for step in server.scenario:
                step_type_id = None
                declare_blob = None
                if step.step_type is StepType.SYM:
                    step_type_id = self.step_types_sym
                    pass
                elif step.step_type is StepType.Questionnaire:
                    step_type_id = self.step_types_quest
                    declare_blob = step.pattern_path
                else:
                    continue
                pref_step = f"""insert into Step (step_type, step_index, declaration, _experiment)
                                values ({step_type_id},
                                {step.step_rank},
                                '{declare_blob}',
                                {exp_id})"""
                # print(pref_step)
                cr.execute(pref_step)
                _con.commit()
                print(f"""_experiment == {self.id_experiment} and step_index == {step.step_rank}""")
                res = cr.execute(f"""select id_step from Step 
                                     where _experiment = {self.id_experiment} 
                                     and step_index = {step.step_rank}""")
                self.steps.append([step.step_rank, res.fetchone()[0]])
                _con.commit()
            _con.close()
        except sqlite3.Error as e:
            display_error(e)
        return
    
    
    def verify_db(self) -> None:
        """
        Verifies the presence of basic tables
        # TODO could be more complete
        :return:
        """
        _con = sqlite3.connect(self.target)
        cr = _con.cursor()
        # check tables names
        table_names = [
            'Step_type',
            'User',
            'Questionnaire',
            'Experiment',
            'Step',
            'Sym_data'
        ]
        res = cr.execute("""select name from sqlite_master""")
        tables_present = [n[0] for n in res.fetchall()]
        for name in table_names:
            if name not in tables_present:
                raise AssertionError
        _con.close()
        return
    
    
    def create_blank(self) -> None:
        """
        Creates a new database set
        :return: connection and cursor objects for created db
        """
        try:
            _con = sqlite3.connect(self.target)
            cr = _con.cursor()

            table_step_type = """
            create table Step_type
            (
                s_type    TEXT    not null,
                id_s_type integer not null
                    constraint Step_type_pk
                        primary key autoincrement
            );"""
            populate_step_type = """
            insert into Step_type (s_type) values ('SYM');
            insert into Step_type (s_type) values ('Questionnaire');
            """
            table_experiment = """
            create table Experiment
            (
                id_experiment integer not null
                    constraint Experiment_pk
                        primary key autoincrement,
                name          text,
                description   text,
                video_start   integer,
                video_end     integer
            );
            """
            table_user = """
            create table User
            (
                id_user     integer not null
                    constraint User_pk
                        primary key,
                _experiment integer not null
                    constraint User_Experiment_id_experiment_fk
                        references Experiment(id_experiment)
            );"""
            table_steps = """
            create table Step
            (
                id_step     integer              not null
                    constraint Step_pk
                        primary key autoincrement,
                step_type   integer              not null
                    constraint Step_Step_type_id_s_type_fk
                        references Step_type(s_type),
                step_index  integer default NULL not null,
                declaration text    default '',
                _experiment integer              not null
                    constraint Step_Experiment_id_experiment_fk
                        references Experiment(id_experiment)
            );"""
            table_questionnaires = """
            create table Questionnaire
            (
                id_questionnaire integer not null
                    constraint Questionnaire_pk
                        primary key autoincrement,
                path          BLOB,
                _step            integer not null
                    constraint Questionnaire_Step_id_step_fk
                        references Step(id_step)
            );"""
            table_sym_data = """
            create table Sym_data
            (
                id_sym_data integer not null
                    constraint Sym_data_pk
                        primary key autoincrement,
                x           integer not null,
                y           integer not null,
                _user       integer not null
                    constraint Sym_data_User_id_user_fk
                        references User(id_user),
                timestamp   integer not null,
                _step       integer not null
                    constraint Sym_data_Step_id_step_fk
                        references Step(id_step)
            );"""
            table_question = """
            create table Question
            (
                id_question integer not null
                    constraint Question_pk
                        primary key autoincrement,
                parent integer not null
                    constraint Question_User_id_questionnaire_fk
                        references Questionnaire(id_questionnaire),
                q_type      text    not null,
                q_tag       text    not null,
                q_text      text,
                q_choices   text,
                q_min       text,
                q_max       integer
            );"""
            table_answer = """
            create table Answer
            (
                id_answer integer not null
                    constraint Ans_pk
                        primary key autoincrement,
                ans_question integer not null
                    constraint Ans_Question_id_question_fk
                        references Question(id_question),
                ans_user integer not null
                    constraint Ans_User_id_user_fk
                        references User(id_user),
                text text,
                integer integer,
                extra text,
                array text
            );"""

            cr.executescript(table_step_type)
            cr.executescript(populate_step_type)
            cr.executescript(table_experiment)
            cr.executescript(table_user)
            cr.executescript(table_steps)
            cr.executescript(table_questionnaires)
            cr.executescript(table_sym_data)
            cr.executescript(table_question)
            cr.executescript(table_answer)
            _con.commit()
            _con.close()
        except sqlite3.Error as error:
            display_error(error)
        return


def display_error(e: sqlite3.Error) -> None:
    print('SQLite error: %s' % (' '.join(e.args)))
    print("Exception class: ", e.__class__)
    print('SQLite traceback: ')
    exc_type, exc_value, exc_tb = sys.exc_info()
    print(traceback.format_exception(exc_type, exc_value, exc_tb))


if __name__ == '__main__':
    # testing

    # create the DB
    dbe = DBE()
    dbe.create_blank()
    dbe.verify_db()

    # insert Experiment
    server = Server()
    server.load_scenario('../scenarios/scenario.json')
    dbe.declare_experiment(server)

    # insert data
    dbe.insert_video_start(1000)
    dbe.insert_video_end(2000)

    _user = User(1200)
    dbe.insert_user_id(_user)

    sd = SYMData({
        'x': 0,
        'y': 0,
        'id': _user.user_id,
        'timestamp': 2500
    })
    dbe.insert_sym_data(sd, 1)
    pass
