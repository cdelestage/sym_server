"""
SYM server questionnaire generator
Every questionnaire is declared as a JSON file in the quest_templates folder
This scripts allows to generate the html page in the template static folder
"""

import json
import core

__author__ = "Charles-Alexandre Delestage"
__copyright__ = "2021-2024"
__credits__ = ["Charles-Alexandre Delestage"]
__license__ = "BSD-3"
__version__ = "0.2"
__maintainer__ = "Charles-Alexandre Delestage"
__email__ = "charles-alexandre.delestage@iut.u-bordeaux-montaigne.fr"
__status__ = "Development"


class Questionnaire(object):

    def __init__(self, step_id: int, page_name='', json_path: str=None, dbe=None):
        """
        Questionnaire declaration
        Could be from JSON file or from blank
        :param page_name: no idea why it can be declared here
        :param json_path: relative path to the JSON template file
        """
        self.html = """"""
        self.mandatory: list[tuple] = []
        self.live_save = dbe is not None
        if self.live_save:
            self.dbe = dbe
            self.quest_id = step_id
        if json_path:
            self.from_json(json_path)
            return
        self.page_name = page_name

    def from_json(self, json_file) -> None:
        """
        Heart of the stuff - generates html from the JSON template typology
        Relies on sub-functions for readability (one for each type and specials)
        :param json_file: relative path to the JSON file
        """
        with open(json_file, 'r') as f:
            data = json.load(f)
            choices = 'choices'
            question = 'question'
            min_tag = 'min_tag'
            max_tag = 'max_tag'
            title = 'title'
            intro = 'intro'
            self.page_name = data['name']
            self.add_head(data['head']['style'], data['head'][title])
            if data['form']:
                self.add_form_start(data['form']['action'])
                for section in data['form']['sections']:
                    if section['type'] == 'simple':
                        self.add_simple_text(section['tag'],
                                             section[question],
                                             'mandatory' in section)
                        if self.live_save:
                            self.dbe.insert_question(self.quest_id, 'simple', section['tag'], section[question])
                    if section['type'] == 'title':
                        self.add_title(section[title],
                                       section[intro])
                    if section['type'] == 'check':
                        self.add_check(section['tag'],
                                       section[question],
                                       section[choices],
                                       'extras' in section,
                                       section['extras'] if 'extras' in section else None,
                                       'mandatory' in section)
                        if self.live_save:
                            self.dbe.insert_question(self.quest_id, 'check', section['tag'], section[question],
                                                     choices=section[choices])
                    if section['type'] == 'radio':
                        self.add_radio(section['tag'],
                                       section[question],
                                       section[choices],
                                       'mandatory' in section)
                        if self.live_save:
                            self.dbe.insert_question(self.quest_id, 'radio', section['tag'], section[question],
                                                     choices=section[choices])
                    if section['type'] == 'vas':
                        self.add_vas(section['tag'],
                                     section[min_tag],
                                     section[max_tag],
                                     section[question])
                        if self.live_save:
                            self.dbe.insert_question(self.quest_id, 'vas', section['tag'], section[question],
                                                     _min=section[min_tag], _max=section[max_tag])
                    if section['type'] == 'comment':
                        self.add_comment(section['tag'],
                                         section[question],
                                         'mandatory' in section)
                        if self.live_save:
                            self.dbe.insert_question(self.quest_id, 'comment', section['tag'], section[question])
            self.add_verification()
            self.finalize(data['validation']).save()
            self.html = """"""

    def add_comment(self, tag: str, question: str, mandatory: bool = False):
        """
        Adds an open question with text field
        :param mandatory: true if question is to be answered
        :param tag: tag of the question for data analysis
        :param question: text of the question to be asked
        :return: self
        """
        if mandatory:
            self.mandatory.append((tag, 'text'))
        self.html += f"""
        <div><h3>{question}</h3>
            <textarea id="{tag}" name="{tag}" rows="4" cols="50"></textarea>
        </div>
        """
        return self

    def save(self) -> None:
        """
        Exports the generated HTML in memory to an HTML file in the templates static folder
        """
        with open('templates/' + self.page_name + '.html', 'wb') as f:
            f.write(self.html.encode('utf-8'))

    def finalize(self, next_label):
        """
        Adds the ending form marker and the end of the HTML file
        :param next_label: tag of the submit label
        :return: self
        """
        self.html += """
        <div class="submitcontainer">
            <input type="submit" id="b_submit" value="NEXT_LABEL">
        </div>
        </form>
        <script>
            $(document).ready(function() {
                $(window).keydown(function(event){
                    if(event.keyCode === 13) {
                        event.preventDefault();
                        return false;
                    }
                });
            });
        </script>
        </body>
        </html>""".replace('NEXT_LABEL', next_label)
        return self

    def add_simple_text(self, tag: str, question: str, mandatory: bool = False):
        """
        Adds a short text answer field
        Usually for giving an ID for single server mode, should disappear ? Maybe
        :param mandatory: true if question is to be answered
        :param tag: tag of the question for data analysis
        :param question: text of the question to be asked
        :return: self
        """
        if mandatory:
            self.mandatory.append((tag, 'text'))
        prebuild = f"""
                <div><h3>{question}</h3>
                <div class="simple_text">
                <div class="textinputdiv"><input type="text" id="{tag}" name="{tag}" cols="40" rows="4"></div>
            </div>
            </div>
            """
        self.html += prebuild
        return self

    def add_title(self, title, intro) -> object:
        """
        Adds a title for the questionnaire / section
        :param title: title of the questionnaire / section
        :param intro: optional introduction text
        :return: self
        """
        self.html += f"""
        <h1>{title}</h1>
        <p>{intro}</p>
        """
        return self

    def add_form_start(self, reroute) -> object:
        """
        Adds the beginning of the form marker
        :param reroute: redirection action and tag (see bellow)
        :return: self
        """
        self.html += f"""
        <form method="post" action="/quest_send" name="{reroute[1:]}" id="sym_server_form">
        """
        return self

    def add_head(self, static_css, main_title) -> object:
        """
        Adds the head of the HTML file
        :param static_css: path of the css with Flask integration
        :param main_title: title of the page
        :return: self
        """
        prebuild = f"""<!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <title>{main_title}</title>
                <link rel="stylesheet" href="{{{{url_for('static', filename='{static_css}')}}}}"/>
                <script src="{{{{url_for('static', filename='js/jquery-3.7.1.min.js')}}}}"></script>
                <script src="{{{{url_for('static', filename='js/jquery-ui.js')}}}}"></script>
            </head>
            <body>
            """
        self.html += prebuild
        return self

    def add_section(self, title) -> object:
        """
        Adds a section to the questionnaire
        :param title: title of the section
        :return: self
        """
        self.html += f"""
        <hr>
        
        <h2>{title}</h2>
        """
        return self

    def add_check(self, tag: str,
                  question: str,
                  choices: list[str],
                  extras: bool = False,
                  extras_label: str = None,
                  mandatory: bool = False):
        """
        Adds a checkbox question (multiple choices)
        The tag is for the question and the responses (tag for answers is TAG_NBR)
        :param mandatory: true if question is to be answered
        :param tag: tag of the question
        :param question: text of the question
        :param choices: list of choices as an array
        :param extras: option for textfield for "Other" type of answers
        :param extras_label: text for optional extra answer
        :return: self
        """
        if mandatory:
            self.mandatory.append((tag, 'check'))
        prebuild_check = """
        <div class="check_el">
            <input type="checkbox" id="TAG_NBR" name="TAG" value="NBR"><label for="TAG_NBR">CHOICE</label></div>"""
        result = """
        <div class="check">
        <h3>""" + question + """</h3>"""
        i = 0
        for c in choices:
            temp = prebuild_check
            temp = temp.replace('TAG', tag).replace('NBR', str(i)).replace('CHOICE', c)
            result += temp
            i += 1
        if extras:
            result += f"""<div class="textextras">{extras_label}
                <div class="textinputdiv">
                    <input type="text" id="{tag}_extra" name="{tag}_extra" cols="40" rows="4">
                </div>
            </div>
            """
        result += """</div>
        """
        self.html += result
        return self

    def add_radio(self, tag: str, question: str, choices: list[str], mandatory: bool = False):
        """
        Adds a radio question (single choice)
        The tag is for the question and the responses (tag for answers is TAG_NBR)
        :param mandatory: true if question is to be answered
        :param tag: tag of the question
        :param question: text of the question
        :param choices: list of choices as an array
        :return: self
        """
        if mandatory:
            self.mandatory.append((tag, 'radio'))
        prebuild_radio = """
        <div class="radio_el">
            <input type="radio" id="TAG_NBR" name="TAG" value="NBR"><label for="NBR">CHOICE</label></div>"""
        result = """
        <div class="radio">
        <h3>""" + question + """</h3>"""
        i = 0
        for c in choices:
            temp = prebuild_radio
            temp = temp.replace('TAG', tag).replace('NBR', str(i)).replace('CHOICE', c)
            result += temp
            i += 1
        result += """</div>
        """
        self.html += result
        return self

    def add_vas(self, tag: str, min_label: str, max_label: str, question: str):
        """
        Adds a Visual Analog Scale (VAS) question
        :param tag: tag of the question
        :param min_label: text for the minimum (left side) of the VAS
        :param max_label: text for the maximum (right side) of the VAS
        :param question: text of the question
        :return: self
        """

        prebuild = f"""
        <div><h3>{question}</h3>
        <div class="vas_container">
        <label for="{tag}" class="vaslabel">{min_label}</label><input type="range" min="1" max="100" value="50" 
        class="slider" id="{tag}" name="{tag}"><label for="{tag}" class="vaslabel">{max_label}</label> 
    </div>
    </div>
    """
        self.html += prebuild
        return self

    def add_verification(self) -> object:
        """
        Adds value response verification for the submit button based on the tags present in the JSON template file
        TODO : make it work !
        :return: self
        """
        # return self
        if len(self.mandatory) == 0:
            return self
        name_list = []
        bool_check = ''
        for tag, q_type in self.mandatory:
            if q_type == 'vas':
                continue
            elif q_type in ['radio', 'check']:
                name_list.append(f"""($("input[name='{tag}']:checked") !== null)""")
            else:
                name_list.append(
                    f"""$(("input[name='{tag}']").val() === \"\")""")
            bool_check = ' && '.join(name_list)
        self.html += """
        <script>
            $('#sym_server_form').submit(function() {
                if (!(""" + bool_check + """)) {
                    alert('Merci de remplir l\\'ensemble des champs marqués d\\'un astérisque');
                    return false;
                }
                else return true;
            });            
        </script>"""
        return self


def generate_from_scenario(dbe=None) -> bool:
    with open('scenarios/scenario.json', 'r') as scenario:
        scenario = json.load(scenario)
        for step in scenario.get('steps'):
            if step.get('type') == 'questionnaire':
                Questionnaire(step.get('order'), json_path='quest_templates/' + step.get('pattern_path'), dbe=dbe)
    return True


if __name__ == '__main__':
    generate_from_scenario()
