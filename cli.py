import analytics
import argparse
import os

if __name__ == '__main__':
    # Set up argument parser
    parser = argparse.ArgumentParser(
        prog='SymAnalytics',
        description='SYM analytics package')

    # Optional filename argument
    parser.add_argument('filename', nargs='?', help='SYM file')

    # Version flag
    parser.add_argument('--version', action='version', version=analytics.__version__)

    # Single flag that requires a filename
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--single', action='store_true', help='Does only the per user diagram generation')
    group.add_argument('--heatmap', action='store_true', help='Does only the heatmap generation')
    group.add_argument('--full', action='store_true', help='Does single and heatmap generation')

    # Parse arguments
    args = parser.parse_args()

    if args.single and not args.filename:
        print("Error: --single flag requires a filename")
        exit(1)
    if args.heatmap and not args.filename:
        print("Error: --heatmap flag requires a filename")
        exit(1)
    if args.full and not args.filename:
        print("Error: --full flag requires a filename")
        exit(1)

    # filename is present, load the SYM file  with the SWV format
    _users = []
    if os.path.isfile(args.filename):
        print('SYM data loading')
        _users = analytics.import_sym_data(args.filename)
        print(f'SYM data loaded: {len(_users)} users found')
    else:
        print(f'{args.filename}: file does not exists')
        exit(1)
    # Call your functions based on the parsed arguments
    if args.single:
        print('Generating single user diagrams')
        for user in _users:
            analytics.generate_diagram([user])
        print('Done')
        exit(0)
    elif args.full:
        print('Generating single user and heatmap')
        for user in _users:
            analytics.generate_diagram([user])
        analytics.sym_data_heatmap(_users, step=5, bary=True, clusters=True)
        print('Done')
        exit(0)
    elif args.heatmap:
        print('Generating heatmap for users')
        analytics.sym_data_heatmap(_users, step=5, bary=True, clusters=True)
        print('Done')
        exit(0)
    args = parser.parse_args()
