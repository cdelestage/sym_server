[English version](README.md) [Version Française](README_FR.md) [Wiki](https://gitlab.huma-num.fr/cdelestage/sym_server/-/wikis/home)

# SYM Server
Une version orientée serveur de Spot Your Mood avec des fonctionnalités de questionnaires avant/après et de lecteur de vidéo.

# Solution matérielle recommandée
SYM Server fonctionne sur le principe client-serveur. Si vous êtes sur une même machine ou un réseau filaire, l'utilisation est relativement simple et classique.

Dans l'idéal, si vous avez accès à un portail WiFi captif, ou la possibilité d'une redirection sur votre réseau WiFi, cela permettra de rediriger les utilisateurs directement sur la page client du serveur.

Sinon, utilisez un couple de QR-Codes pour se connecter au WiFi puis au serveur. D'expérience, c'est une succession d'étapes trop complexe pour les utilisateurs, qui ne sont pas encore habitués aux QR-Codes pour rejoindre des réseaux WiFi, et qui demande un accompagnement technique. C'est par conséquent ingérable en multi-utilisateurs. 

# Configuration logicielle

Voici les versions de python et des dépendances principales utilisées dans le projet : 

* Python>=3.10.7 (3.8 minimum)
* openpyxl=3.1.2
* Flask=2.3.2
* flask_socketio=5.3.4

# Installation et premier lancement
Téléchargez le code via l'archive zip ou clonez le dépôt via git :

```
git clone https://gitlab.huma-num.fr/cdelestage/sym_server
cd sym_server/
```

Le serveur étant basé en python, il est recommandé d'utiliser un environnement virtuel pour l'exécution. 
Nous avons utilisé venv, mais vous pouvez utiliser l'environnement de votre choix. Voici un exemple pour venv :

```
# Si venv n'est pas déjà installé, vous pouvez l'installer avec la commande suivante
pip install virtualenv
# Création de l'environnement virtuel dans le dossier actuel
python -m venv venv

# Activation de l'environnement sous Linux/MacOS
source venv/bin/activate
# Activation de l'environnement sous Windows (cmd.exe)
venv\Scripts\activate.bat
# Activation de l'environnement sous Windows (PowerShell)
venv\Scripts\Activate.psl

# Installation des dépendances
pip install -r requirements.txt

# Lancement du serveur
python app.py
```

# Scénarios modulaires
Vous avez le choix du déroulement du scénario de protocole via une génération par type d'activités.
Il existe plusieurs types d'activités et sous-activités :
* Spot Your Mood, qui peut se faire :
  * En suivi continu (Delestage, 2018), qui peut se dérouler en parallèle d'une autre activité synchronisée ou non avec le serveur. Dans le cas d'une vidéo, il est possible de lancer un player vidéo sur un ordinateur quelconque relié au réseau. Le début et la fin de la vidéo seront consignés dans le serveur ; 
  * En pointage unique (Yvart, 2019).
* Questionnaires, avec différents types de questions :
  * Questions à choix uniques ;
  * Questions à choix multiples ; 
  * Visual Analog Scales ;
  * Champs de textes libre.

Le contenu est éditable à volonté dans le fichier scenario.json (voir le [Wiki](https://gitlab.huma-num.fr/cdelestage/sym_server/-/wikis/home) pour plus de détails).

# Templates de questionnaires
Vous pouvez personnaliser les questionnaires via les templates json situés dans le dossier 'quest_templates'. Des exemples sont fournis.
Consultez le [Wiki](https://gitlab.huma-num.fr/cdelestage/sym_server/-/wikis/home) pour plus de détails.

# Dépendances
SYM Server est basé sur les librairies suivantes pour la gestion des données et la partie réseau (merci à leurs créateurs pour rendre ces projets possibles !).

Python
* [openpyxl 3.1.2](https://github.com/ericgazoni/openpyxl)
* [Flask 2.3.2](https://github.com/pallets/flask)
* [flask_socketio 5.3.4](https://github.com/miguelgrinberg/Flask-SocketIO)

Javascript
* [Bootstrap 5.2.2](https://github.com/twbs/bootstrap)
* [JQuery 3.7.1](https://github.com/jquery/jquery)
* [SocketIO 4.0.1](https://github.com/socketio/socket.io)
* [Konva 8.1.3](https://github.com/konvajs/konva)

Fonts
* [DejaVu](https://dejavu-fonts.github.io/)

# Licence
Le code du projet est sous licence BSD-3. Consultez la [LICENCE]() pour plus de détails.

# Articles sur SYM
Afin d'avoir plus d'informations sur l'usage de SYM, vous pouvez consulter les articles suivants :
* DELESTAGE, Charles-Alexandre et YVART, Willy, 2024. Lived experience and virtual reality: visual method of analysis based on video recordings and the Valence-Arousal diagram. Sintaxis. 15 janvier 2024. N° 12, pp. 68‑85. DOI [10.36105/stx.2024n12.07](https://doi.org/10.36105/stx.2024n12.07).
* DELESTAGE, Charles-Alexandre et YVART, Willy, 2023. Visualisation des transferts émotionnels de cohortes – Étude de cas sur la notion de non-violence. In : H2PTM’23, La fabrique du sens à l’ère de l’information numérique : enjeux et défis. Imad Saleh, Nasreddine Bouhaï, Sylvie Leleu-Merviel, Ioan Roxin. ISTE Editions. pp. 83‑98. Systèmes d’information, web et société. ISBN 978-1-78405-983-5.
* MOUTAT, Audrey, DELESTAGE, Charles-Alexandre and BADULESCU, Cristina, 2019. Sensorialité et émotions, vers une co-construction du sens : approche qualitative de l’expérience de visite à la Cité du vin de Bordeaux. In: 87e congrès de l’ACFAS. Montréal. 2019.
* YVART, Willy, 2019. Qualification (a)verbale de l’humeur musicale : nouvelles perspectives pour la synchronisation dans l’audiovisuel. 2019. P. 911. 
* DELESTAGE, Charles-Alexandre, 2018. Paths created by an enactive-relativized approach to experience: the case of the viewing experience. In: From UXD to LivXD. Living eXperience Design. Leleu-Merviel, S. (ed). Wiley. 
* DELESTAGE, Charles-Alexandre, 2018. L’expérience émotionnelle ou la performance des programmes de télévision. L’horizon de pertinence comme déterminant de la construction de sens par le spectateur. Thèse de doctorat. Arenberg Creative Mine. 
* DELESTAGE, Charles-Alexandre and YVART, Willy, 2018. Emotional dynamics and quality of lived experience: a study of the usage of the Valence-Arousal space with the Spot Your Mood tool. In: Visual methods for research in the fields of communication. Tenerife, Spain. December 2018.
* YVART, Willy, DELESTAGE, Charles-Alexandre and LELEU-MERVIEL, Sylvie, 2016. SYM: toward a new tool in user’s mood determination. In: Proceedings of the 2016 EmoVis Conference on Emotion and Visualization. Online. Linkoping University. 2016. p. 23–28. Available from: http://dl.acm.org/citation.cfm?id=3001319