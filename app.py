"""
SYM Server

Check the wiki for more info @ https://gitlab.huma-num.fr/cdelestage/sym_server/-/wikis/home
# TODO add a mechanism to say when the database connection is lost
"""

from datetime import datetime
from flask import Flask, render_template, make_response, request, redirect
from flask_httpauth import HTTPBasicAuth
from flask_socketio import SocketIO
import core
from analytics import analytics
from pathlib import Path
import random
import pickle
import logging
import secrets

__author__ = "Charles-Alexandre Delestage"
__copyright__ = "2021-2024"
__credits__ = ["Charles-Alexandre Delestage"]
__license__ = "BSD-3"
__version__ = "0.2"
__maintainer__ = "Charles-Alexandre Delestage"
__email__ = "charles-alexandre.delestage@iut.u-bordeaux-montaigne.fr"
__status__ = "Development"

# Verifies folders structures
def check_hierarchy():
    folders = ['backups',
               'exports',
               'exports/sym',
               'exports/sym/archive',
               'exports/sym/individuels',
               'exports/figures',
               'logs']
    for folder in folders:
        Path(folder).mkdir(parents=True, exist_ok=True)
    # remove old main.py file which is not used anymore
    Path('.main.py').unlink(missing_ok=True)


check_hierarchy()

# DB handler
dbe = core.DBE()
dbe.create_blank()

# Flask variables declaration
app = Flask(__name__)
app.config['SECRET_KEY'] = secrets.token_urlsafe(64)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = -1
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.debug = True
socketio = SocketIO(app, logger=True, engineio_logger=True)
server = core.Server()
server.load_scenario()

dbe.declare_experiment(server)
core.generate_from_scenario(dbe)

# logging
log_name = 'logs/' + datetime.now().strftime('%y-%m-%d_%H-%M-%S') + '.log'
logging.basicConfig(filename=log_name, level=logging.DEBUG)

# An argument could be done on the level of security of this approach - which is not top-notch to say the least
# Thing is, this server is to be used in a "lab setting", which means a dedicated network of some sort
# Probability of a breach is pretty low, especially if you consider that the server should normally run only
# 1h at most per session, on a public which does not have Pro HackerZ Tools 1.3.37 on their phone
# To paraphrase a famous physical security youtuber, this level of security if more of a deterrent for normal people
# and a way to not have it completely open
# If you reuse this project, you know you have to update this part for any kind of other context
auth = HTTPBasicAuth()
print('Security password for control room: ' + str(server.control_key))


@auth.verify_password
def verify_password(_, password):
    if secrets.compare_digest(password, server.control_key):
        return True


######################################
#   Default redirection for users    #
######################################


@app.route('/')
def base():
    """
    Root redirection
    Checks the presence of the tracking cookie and redirects the user accordingly
    It's not the best method for security reasons (cookie is transmitted in clear and is the only 'authentication'
    method which could be easily hijacked), but it's efficient enough for now.
    :return: page to be displayed regarding state of the experiment
    """
    name = request.cookies.get('SYMuserID')
    date_now = datetime.now().strftime('%y-%m-%d_%H-%M-%S')

    cast_stats()

    if name is None:  # new user => redirection to consent form
        logging.info(f'[{date_now}] New user connected')
        return render_template('engagement.html')
    else:  # existing user => estimate redirection based on id
        current_status = server.get_user_status(int(name))  # grabbing current user step
        if current_status is None: # failsafe
            return render_template('engagement.html')
        if current_status.step_type == core.StepType.End:  # if finished, gets to the thanks page
            user_id = request.cookies.get('SYMuserID')
            resp = make_response(render_template('remerciements.html', continuity_number=user_id))
            logging.info(f'[{date_now}] User cookie deleted: ' + user_id)
            resp.set_cookie('SYMuserID', '', expires=0)
            return resp
        else:  # default case, redirection based on current Step
            logging.info(f'[{date_now}] User {name} reaching {current_status.template_path}')
            # If it's a SYM Step, pass the single_mode information to the page
            if current_status.step_type == core.StepType.SYM: # special parameters for SYM
                assert isinstance(current_status, core.SYMPage)
                str_mode = str(current_status.single_mode).lower()
                hold_mode = str(current_status.hold).lower()
                return render_template(current_status.template_path, single_mode=str_mode, hold=hold_mode)
            return render_template(current_status.template_path)


######################
#   Static paths     #
######################


@app.route('/control_room')
# @auth.login_required
def ctrl():
    """
    Displays the control room
    This is the interface of the researcher to control the experiment and monitor data collection
    :return: page of the control room
    """
    cast_stats()
    return make_response(render_template('control_room.html', init_stats=cast_stats(True)))


@app.route('/video_player')
def play_video():
    """
    Displays the video page.
    :return: page of the video to be displayed
    """
    return render_template('video_player.html', video_url=server.video_url)


#########################################
#   Data I/O handling from clients      #
#########################################


@app.route('/register', methods=['POST', 'GET'])
def register_user():
    """
    Registers the user in the server
    :return: page of the appropriate pre questionnaire
    """
    # generate random id (16 bits should be enough for not having 2 users with the same id)
    # change to upper value if needed
    new_user_id = str(random.getrandbits(16))
    # inject user in the server object
    server.experiment.add_user(int(new_user_id))
    server.next_user_step(int(new_user_id), cast_stats)
    # prepare page
    resp = make_response(render_template(server.get_user_status(int(new_user_id)).template_path))
    # cookie set to track users during the experiment
    # of course, you have the responsibility to ensure that users don't block cookies ! (they generally don't)
    resp.set_cookie('SYMuserID', new_user_id, samesite='Lax')
    # logging
    date_now = datetime.now().strftime('%y-%m-%d_%H-%M-%S')
    logging.info(f'[{date_now}] User starting: {new_user_id}')
    dbe.insert_user_id(server.experiment.get_user(int(new_user_id)))
    return resp


@socketio.on('data_send')
def handle_data_send(data, hold: bool = False):
    """
    Handles the SYM data reception from clients
    Timestamp is generated upon reception server-side in Unix time (ms)
    Date is saved in memory and in the backup text file
    :param data: SYM data in dict
    :param hold: if the page is in hold mode or not (which will condition redirection)
    """
    # assert isinstance(data, dict)
    # ping back for confirmation
    socketio.emit('data_recv', data)
    # local timestamp, to remove when time offsets are in effect
    date_now = datetime.now()
    data['timestamp'] = date_now.timestamp() * 1e6
    # pack and ingest data
    sd = core.SYMData(data)
    server.experiment.add_sym_data(sd)
    # logging
    time_now = date_now.strftime('%y-%m-%d_%H-%M-%S')
    logging.info(f'[{time_now}] SYM data received: {str(sd)}')
    dbe.insert_sym_data(sd, server.get_user_status(int(sd.user_id)).step_rank)
    print(data)
    # handle redirection
    if not hold:
        user_id = request.cookies.get('SYMuserID')
        server.next_user_step(int(user_id), cast_stats)
        print(server.experiment.get_user(int(user_id)).status)
        socketio.emit('sym_reload', '')


@app.route('/quest_send', methods=['POST'])
def handle_quest_return():
    """
    Handles data from the pre questionnaire page
    Data is stored in the backup text file and in memory within objects
    :return: redirects to the sym page
    """
    # get user id and ingest
    user_id = request.cookies.get('SYMuserID')
    server.experiment.add_quest_data(core.Quests(request.form), int(user_id))
    # logging
    date_now = datetime.now().strftime('%y-%m-%d_%H-%M-%S')
    logging.info(f'[{date_now}] User questionnaire received: {user_id}')
    # update user's step and redirect
    server.next_user_step(int(user_id), cast_stats)
    # data backup in case of
    timestamp = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    with open('backups/data_save' + timestamp + '.pickle', 'wb') as save:
        pickle.dump(server.experiment, save, pickle.HIGHEST_PROTOCOL)
    dbe.insert_quest_blob(server.get_user_status(int(user_id)).step_rank,  int(user_id), request.form)
    return redirect('/')


##################################
#   Server remote management     #
##################################


@socketio.on('next_step')
def handle_next_step(msg):
    """
    Broadcasts an order to all client as to move to the next step
    :param msg: does not matter in current usage, an 'in case of'
    """
    # switch ever user to post-SYM step server-side
    server.global_next_switch()
    # broadcast to users
    socketio.emit('broadcast_next_step', msg, broadcast=True)
    # logging
    time_now = datetime.now().strftime('%y-%m-%d_%H-%M-%S')
    logging.info(f'[{time_now}] Switch to post-questionnaire order sent')


def cast_stats(return_value=False):
    """
    Gives input on the users' current steps
    Only for the control room, as a way to know where everyone is at
    Is to be triggered when stats evolve (see usage of Server.next_user_step()).
    :param return_value: False if only needed to send it to the control room (general case), True if needed for
    control_room initialization of the scenarios steps.
    """
    _steps_user_counts: [[int, str, str]] = []
    for step in server.scenario:
        _step_count = 0
        # count users per step
        for user in server.experiment.users:
            if user.status == step:
                _step_count += 1
        # appends count, Step full name and Step safe name for html id tag
        _steps_user_counts.append([_step_count, step.name, str(step.name).lower().replace(' ', '_')])
    time_now = datetime.now().strftime('%y-%m-%d_%H-%M-%S')
    logging.info(f'[{time_now}] Steps stats updated')
    if not return_value:
        socketio.emit('counts', _steps_user_counts)
    else:
        return _steps_user_counts


@socketio.on('backup_data')
def handle_total_backup():
    """
    Makes a data backup in pickle format of the data in memory (see Experiment object for details)
    """
    timestamp = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    logging.debug(f'[{timestamp}] Data backup order received')
    with open('backups/data_save' + timestamp + '.pickle', 'wb') as save:
        pickle.dump(server.experiment, save, pickle.HIGHEST_PROTOCOL)
    # TODO check for actual confirmation
    socketio.emit('backup_confirmed', '')
    timestamp = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    logging.debug(f'[{timestamp}] Data backup order executed successfully')


@socketio.on('video_start')
def handle_video_start():
    """
    Saves the start of the video
    Same server side timestamp generation problems
    Low priority since it is usually on the same machine
    """
    date_now = datetime.now()
    print('video started ' + str(date_now.timestamp() * 1e6))
    socketio.emit('video_start_ctl')
    server.experiment.set_reel_start(date_now.timestamp() * 1e6)
    time_now = date_now.strftime('%y-%m-%d_%H-%M-%S')
    logging.debug(f'[{time_now}] Video started')


@socketio.on('video_end')
def handle_video_end():
    """
    Saves the end of the video
    Same server side timestamp generation problems
    Low priority since it is usually on the same machine
    """
    date_now = datetime.now()
    print('video ended ' + str(date_now.timestamp() * 1e6))
    socketio.emit('video_end_ctl')
    server.experiment.set_reel_end(date_now.timestamp() * 1e6)
    time_now = date_now.strftime('%y-%m-%d_%H-%M-%S')
    logging.debug(f'[{time_now}] Video ended')


@socketio.on('video_paused')
def handle_video_paused():
    date_now = datetime.now()
    print('video paused ' + str(date_now.timestamp() * 1e6))
    socketio.emit('video_paused_ctl')
    server.experiment.add_reel_pause(date_now.timestamp() * 1e6)
    time_now = datetime.now().strftime('%y-%m-%d_%H-%M-%S')
    logging.debug(f'[{time_now}] Video paused')


@socketio.on('req_play')
def handle_ctl_play():
    socketio.emit('srv_play')


@socketio.on('req_pause')
def handle_ctl_pause():
    socketio.emit('srv_pause')


@socketio.on('erase_all_data')
def erase_data():
    time_now = datetime.now().strftime('%y-%m-%d_%H-%M-%S')
    logging.debug(f'[{time_now}] Data in memory erase requested')
    server.experiment = core.Experiment()


@socketio.on('stop_server')
def stop_server():
    socketio.stop()
    time_now = datetime.now().strftime('%y-%m-%d_%H-%M-%S')
    logging.debug(f'[{time_now}] Server stop order received')
    exit(0)


@socketio.on('generate_data')
def generate_data():
    """
    Generates diagrams and excel export file for the entire experiment
    Questionnaires will be all in the Excel file @ 'exports' folder
    Diagrams will be generated for each user and the cumulated dataset @ 'exports/sym' folder
    """
    # export the Questionnaire data
    core.export_to_file(server.experiment.users,
                   datetime.now().strftime('%y-%m-%d_%H-%M-%S'),
                   auto_rebuilt=True)
    # global SYMData export
    analytics.generate_diagram(server.experiment.users)
    # per user SYMData export
    for user in server.experiment.users:
        analytics.generate_diagram([user])
    # TODO add display on control room
    socketio.emit('generate_success')


def run():
    socketio.run(app, host='0.0.0.0', port=5000)


if __name__ == '__main__':
    run()
